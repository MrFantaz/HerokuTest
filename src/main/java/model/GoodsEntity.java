package model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "goods", schema = "public", catalog = "d3lt7eju0rnvls")
public class GoodsEntity {
    private int id;
    private String title;
    private String category;
    private String descripton;
    private String url;
    private Timestamp startRate;
    private Timestamp endRate;
    private double price;
//    private Integer userSeller;

    public GoodsEntity() {
    }

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "category")
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Basic
    @Column(name = "descripton")
    public String getDescripton() {
        return descripton;
    }

    public void setDescripton(String descripton) {
        this.descripton = descripton;
    }

    @Basic
    @Column(name = "url")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Basic
    @Column(name = "start_rate")
    public Timestamp getStartRate() {
        return startRate;
    }

    public void setStartRate(Timestamp startRate) {
        this.startRate = startRate;
    }

    @Basic
    @Column(name = "end_rate")
    public Timestamp getEndRate() {
        return endRate;
    }

    public void setEndRate(Timestamp endRate) {
        this.endRate = endRate;
    }

    @Basic
    @Column(name = "price")
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

 //TODO I don't know how resolve reverseColumn
    @ManyToOne(fetch = FetchType.EAGER,optional=true)
    @JoinTable(name = "USERS", joinColumns = @JoinColumn(name = "ID"))
    private UsersEntity userSeller;
    public UsersEntity getUserSeller() {
        return userSeller;
    }

    public void setUserSeller(UsersEntity userSeller) {
        this.userSeller = userSeller;
    }

//    @Basic
//    @Column(name = "user_seller")
//    public Integer getUserSeller() {
//        return userSeller;
//    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GoodsEntity that = (GoodsEntity) o;

        if (id != that.id) return false;
        if (Double.compare(that.price, price) != 0) return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;
        if (category != null ? !category.equals(that.category) : that.category != null) return false;
        if (descripton != null ? !descripton.equals(that.descripton) : that.descripton != null) return false;
        if (url != null ? !url.equals(that.url) : that.url != null) return false;
        if (startRate != null ? !startRate.equals(that.startRate) : that.startRate != null) return false;
        if (endRate != null ? !endRate.equals(that.endRate) : that.endRate != null) return false;
        if (userSeller != null ? !userSeller.equals(that.userSeller) : that.userSeller != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (category != null ? category.hashCode() : 0);
        result = 31 * result + (descripton != null ? descripton.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (startRate != null ? startRate.hashCode() : 0);
        result = 31 * result + (endRate != null ? endRate.hashCode() : 0);
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (userSeller != null ? userSeller.hashCode() : 0);
        return result;
    }
}
