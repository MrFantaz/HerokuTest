package servlets;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import java.io.IOException;
import java.sql.*;

@WebServlet("/connectBD")
public class ConnectBDServlet extends javax.servlet.http.HttpServlet {
    Connection connection = null;

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://ec2-54-243-253-24.compute-1.amazonaws.com:5432/d3lt7eju0rnvls?ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory",
                    "eygcpqilbxijom", "c65b737c10e9364f90419b0e45736ca331572f6176d3020a1a9356422d77424b");
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM firsttable;");
            while (rs.next()){
                System.out.println("title "+rs.getString("title"));
            }
            connection.close();
            System.out.println("We are here");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

    }
}
